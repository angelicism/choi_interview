#Producing:
#
#e.g.
#bundle exec rails runner ./bin/kafka_producer.rb --bay-count 5000 --occupied-time 600
#bundle exec rails runner ./bin/kafka_producer.rb --bay-count 100 --kafka-host 192.168.10.20 --kafka-port 909
#
#Consuming:
#
#bays: 
#consumer = Poseidon::PartitionConsumer.new("my_test_consumer", "localhost", 9092, 'bays', 0, :earliest_offset)
#loop { messages = consumer.fetch; messages.each { |m| puts m.value } }
#
#bay_events:
#consumer = Poseidon::PartitionConsumer.new("my_test_consumer", "localhost", 9092, 'bay_events', 0, :earliest_offset)
#loop { messages = consumer.fetch; messages.each { |m| puts m.value } }

require 'poseidon'
require 'optparse'

class Bay < Hash
  def initialize(options={})
    defaults = {out_of_service: false, occupied: false, vacant_cycles: 0, occupied_cycles: 0, in_service_cycles: 0, out_of_service_cycles: 0}
    defaults.merge(options).each_pair { |k,v| self[k] = v }
  end

  def occupy(producer)
    producer.send_messages('bay_events', [Event.entry(self)])
    self[:occupied] = true 
    self[:occupied_cycles] = 0
    self[:vacant_cycles] = 0
  end

  def occupy?
    self[:vacant_cycles] > rand(self[:vacant_time])
  end

  def occupied?
    self[:occupied]
  end

  def vacate(producer)
    producer.send_messages('bay_events', [Event.exit(self)])
    self[:occupied] = false
    self[:occupied_cycles] = 0
    self[:vacant_cycles] = 0
  end

  def vacate?
    self[:occupied_cycles] > rand(self[:occupied_time])
  end

  def still_occupied
    self[:occupied_cycles] += 1
  end

  def still_vacant
    self[:vacant_cycles] += 1
  end

  def go_out_of_service
    self[:out_of_service] = true 
    self[:in_service_cycles] = 0
    self[:out_of_service_cycles] = 0
  end

  def back_in_service
    self[:out_of_service] = false
    self[:in_service_cycles] = 0
    self[:out_of_service_cycles] = 0
  end

  def still_in_service
    self[:in_service_cycles] += 1
  end

  def still_out_of_service
    self[:out_of_service_cycles] += 1
  end

  def in_service?
    !self[:out_of_service]
  end

  def out_of_service?
    self[:out_of_service]
  end

  def to_s
    {id: self[:id], occupied: self[:occupied], out_of_service: self[:out_of_service]}.to_s
  end
end

class Event < Hash
  def initialize(options={})
    options.each_pair { |k,v| self[k] = v }
  end

  def self.entry(bay)
    new({:bay_id => bay[:id], timestamp: DateTime.now.to_s, type: 'entry'})
  end

  def self.exit(bay)
    new({:bay_id => bay[:id], timestamp: DateTime.now.to_s, type: 'exit'})
  end
end

class Producer
  def initialize(host, port, name='producer')
    @host = host
    @port = port
    @name = name
    client
  end

  def client
    @client ||= Poseidon::Producer.new(["#{@host}:#{@port}"], @name)
  end

  def send_messages(topic, messages)
    if (messages.is_a?(Array) && messages.first.is_a?(Poseidon::MessageToSend)) || messages.is_a?(Poseidon::MessageToSend)
      client.send_messages(messages) 
    else
      client.send_messages(wrap_messages(topic, messages))
    end
  end

  def wrap_messages(topic, messages)
    messages.collect do |message|
      Poseidon::MessageToSend.new(topic, message.to_s)
    end
  end
end

def shut_down
  puts "\nShutting down gracefully..."
  sleep 1
end 

# Trap ^C
Signal.trap("INT") {
shut_down
exit
}
 
# Trap `Kill `
Signal.trap("TERM") {
shut_down
exit
} 

options = {}

options[:kafka_host] = 'localhost'
options[:kafka_port] = 9092
options[:bay_count] = 10
options[:bay_lifespan] = 300
options[:out_of_service_probability] = 0.05
options[:occupied_time] = 600
options[:vacant_time] = 30
options[:high_speed] = false

opt_parser = OptionParser.new do |opts|
  opts.banner = "Usage: kafka_producer.rb [options]" 

  opts.separator ""
  opts.separator "Specific options:"

  opts.on("-k", "--kafka-host [host]", "The host running the kafka server") { |host| options[:kafka_host] = host }
  opts.on("-p", "--kafka-port [port]", "The port accepting connections on the kafka server") { |port| options[:kafka_port] = port }

  opts.on("-b", "--bay-count [count]", "The number of bays in the system") { |bay_count| options[:bay_count] = bay_count.to_i }
  opts.on("-o", "--bay-lifespan [lifespan]", "The length of time a bay can run before going out of service in seconds") { |freq| options[:bay_lifespan] = freq.to_i }
  opts.on("-o", "--out-of-service-probability [prob]", "The chance a bay will go out of service") { |prob| options[:out_of_service_probability] = prob.to_f }
  opts.on("-d", "--occupied-time [occupied-time]", "The maximum time a bay stays occupied in seconds") { |time| options[:occupied_time] = time.to_i }
  opts.on("-a", "--vacant-time [vacant-time]", "The maximum time a bay stays vacant in seconds") { |time| options[:vacant_time] = time.to_i }

  opts.on("-x", "--[no-]high-speed", "The maximum time a bay stays vacant in seconds") { |x| options[:high_speed] = x }
end

opt_parser.parse(ARGV)

"creating producer to communicate with #{options[:kafka_host]}:#{options[:kafka_port]}"
producer = Producer.new(options[:kafka_host], options[:kafka_port], 'producer')

bays = []
puts "creating #{options[:bay_count]} bays"
options[:bay_count].times { |id| bays << Bay.new({id: id, occupied_time: options[:occupied_time], vacant_time: options[:vacant_time]}) }

puts "sending messages to bays topic"
producer.send_messages('bays', bays)

loops = 0
"beginning event loop"
loop do
  entries = 0
  exits = 0
  bays.shuffle.each do |bay|
    if bay.in_service?
      # check if it is time for any bay to be vacanted
      if bay.occupied? 
        if bay.vacate?
          bay.vacate(producer)
          exits += 1
        else
          bay.still_occupied
        end
      # check if it is time for any bay to become occupied
      else
        if bay.occupy?
          bay.occupy(producer)
          entries += 1
        else
          bay.still_vacant
        end
      end

      bay.still_in_service
      if bay[:in_service_cycles] > options[:bay_lifespan] 
        if rand < options[:out_of_service_probability]
          bay.go_out_of_service
          producer.send_messages('bays', [bay])
        end
      end
    else
      bay.still_out_of_service
      if bay[:out_of_service_cycles] > 10
        if rand > options[:out_of_service_probability]
          bay.back_in_service
          producer.send_messages('bays', [bay])
        end
      end
    end
  end

  count = 0
  oos = 0
  bays.each { |bay| count += 1 if bay.occupied? }
  bays.each { |bay| oos += 1 if bay.out_of_service? }
  print "loops: #{loops} occupancy: #{count.to_f/bays.count} entries: #{entries} exits: #{exits} oos: #{oos} \r"

  sleep(1) unless options[:high_speed]
  loops += 1
end
